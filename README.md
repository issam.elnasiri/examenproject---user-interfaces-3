# Examenproject - User Interfaces 3 (Ziekenhuis)

**Web applicatie ter ondersteuning van de verzorging van patiënten in een ziekenhuis.**

## Links projecten op gitlab
Project staat ook op [gitlab](https://gitlab.com/issam.elnasiri/examenproject---user-interfaces-3)
Indien u de server apart wilt downloaden kan u dat doen door [hier](https://gitlab.com/issam.elnasiri/examenproject---ui3---jsonserver) te klikken


## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
