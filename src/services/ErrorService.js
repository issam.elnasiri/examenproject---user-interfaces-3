import { Notify } from 'quasar'

const ErrorService = {
  showAlert (userMessage) {
    Notify.create({
      color: 'negative',
      position: 'bottom',
      message: userMessage,
      icon: 'report_problem'
    })
  },
  log (error) {
    console.log(error)
  }
}

export default ErrorService
