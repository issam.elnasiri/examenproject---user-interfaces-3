import axios from 'axios'

const URL = 'http://localhost:3000/afdelingen'

export default class AfdelingService {
  all () {
    return axios
      .get(URL)
      .then(response => {
        return response.data
      })
  }
  getById (id) {
    return axios
      .get(URL + '/' + id)
      .then(response => {
        return response.data
      })
  }
}
