import axios from 'axios'

const URL = 'http://localhost:3000/kamers'

export default class KamerService {
  all () {
    return axios
      .get(URL)
      .then(response => {
        return response.data
      })
  }
  getById (id) {
    return axios
      .get(URL + '/' + id)
      .then(response => {
        return response.data
      })
  }

  getByPatientId (id) {
    return axios
      .get(URL + '?patient=' + id)
      .then(response => {
        return response.data
      })
  }

  getByAfdelingId (id) {
    return axios
      .get(URL + '?afdelingId=' + id)
      .then(response => {
        return response.data
      })
  }
  getAllHulpKamer () {
    return axios
      .get(URL)
      .then(response => {
        return response.data.filter(HulpKamer)
      })
  }
}

function HulpKamer (value) {
  return value.hulpIngeroepen === true
}
