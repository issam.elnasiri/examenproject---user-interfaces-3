import axios from 'axios'

const URL = 'http://localhost:3000/patienten'

export default class PatientService {
  all () {
    return axios
      .get(URL)
      .then(response => {
        return response.data
      })
  }
  getById (id) {
    return axios
      .get(URL + '/' + id)
      .then(response => {
        return response.data
      })
  }

  update (patient) {
    return axios
      .put('http://localhost:3000/patienten/' + patient.id, patient)
      .then(r => r.status)
  }
}
