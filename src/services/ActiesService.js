import axios from 'axios'

const URL = 'http://localhost:3000/acties'

export default class ActiesService {
  all () {
    return axios.get(URL).then(response => { return response.data })
  }
  getById (id) {
    return axios.get(URL + '/' + id).then(response => { return response.data })
  }

  getByPatientId (id) {
    return axios.get(URL + '?patientId=' + id).then(response => { return response.data })
  }

  delete (actie) {
    return axios.delete(URL + '/' + actie.id)
  }
  add (actie) {
    return axios.post(URL, actie).then(result => result.data)
  }
  update (actie) {
    return axios.put(URL + '/' + actie.id, actie).then(r => r.status)
  }
}
