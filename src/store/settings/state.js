export default function () {
  return {
    list: {
      nummer: true,
      faciliteiten: true,
      patiënt: true,
      monitoringData: true,
      eerstvolgendeActie: true,
      geluid: true,
      darkMode: false,
      minHart: 20,
      maxHart: 100,
      minBloed: 20,
      maxBloed: 100,
      tijd: 120,
      kleuren: {
        vrij: '#ffffff',
        bezet: '#40ff00',
        hulp: '#ff0000',
        gradient1: '#9e4500',
        gradient2: '#ffb780',
        alarm: '#ff0000'
      }
    },
    notifications: []

  }
}
