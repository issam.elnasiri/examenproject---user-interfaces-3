export const setSettings = (state, obj) => {
  Object.assign(state.list, obj)
}

export const pushNotif = (state, obj) => {
  state.notifications.push(obj)
}

export const flushNotif = (state) => {
  state.notifications = []
}
