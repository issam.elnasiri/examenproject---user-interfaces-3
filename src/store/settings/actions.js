export function setSettings (context, obj) {
  context.commit('setSettings', obj)
}

export function pushNotif (context, obj) {
  context.commit('pushNotif', obj)
}

export function flushNotif (context) {
  context.commit('flushNotif')
}
