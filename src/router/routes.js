
const routes = [
  {
    path: '/',
    component: () => import('layouts/HospitalLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'patient/:id', component: () => import('pages/Patient.vue') },
      { path: 'settings', component: () => import('pages/Settings.vue') },
      { path: 'afdeling/:id', component: () => import('pages/Afdeling.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
