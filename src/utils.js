// hulp functies

export default class Utils {
    static getEerstVolgendeActie = (array) => {
      let actie = array.filter(isFalse).sort(function (a, b) {
        return new Date(a.tijdstip) - new Date(b.tijdstip)
      })[0]

      if (actie === undefined) {
        actie = {
          tijdstip: 'geen',
          type: 'geen'
        }
      }
      return actie
    }

    static containsObject (obj, array) {
      for (let i = 0; i < array.length; i++) {
        if (array[i].kamerId === obj.id) {
          return true
        }
      }

      return false
    }

    static removeFromArray (arr, value) {
      return arr.filter(function (ele) {
        return ele !== value
      })
    }

    static removeActieFromArray (arr, value) {
      return arr.filter(function (ele) {
        return ele.id !== value.id
      })
    }

    // hier wordt berekent welke kleur je exact nodig hebt
    static calculateColor (actie, settings, kamer, alarm) {
      let time = calculateTimeLeft(actie.tijdstip)
      let percent = Math.round((time / settings.tijd) * 100)
      if (!alarm) {
        if (time < settings.tijd) {
          if (time < 0) {
            return 'background-color: ' + settings.kleuren.gradient1
          } else if (kamer.hulpIngeroepen) {
            return 'background-color: ' + settings.kleuren.hulp
          } else {
            return 'background-image: linear-gradient(to left,' + settings.kleuren.gradient2 + ', ' + settings.kleuren.gradient1 + ' ' + percent + '%);'
          }
        } else if (kamer.hulpIngeroepen) {
          return 'background-color: ' + settings.kleuren.hulp
        }
      } else {
        return 'background-color: ' + settings.kleuren.alarm
      }
      return 'background-color: ' + settings.kleuren.bezet
    }
}
function calculateTimeLeft (time) {
  let difference = new Date(time) - Date.now()
  return Math.round(difference / 1000 / 60)
}
function isFalse (value) {
  return value.uitgevoerd === false
}
